# ProcessMinder

A simple .NET application which silently runs applications from source.

## Requirements

- Windows 7 SP1 +
- Microsoft .NET framework 4.5
- Visual Studio 2013 (if you wish to modify it)

## License

This project is covered by the [MIT licence](https://opensource.org/licenses/MIT), you can read it [here](https://bitbucket.org/biggerconcept/processminder/blob/master/LICENSE.md).
