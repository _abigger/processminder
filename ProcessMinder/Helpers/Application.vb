﻿Module Application

    Public Property listStyle As View
        Get
            Dim style As View
            With My.Settings
                If (.settingListView = "Details") Then style = View.Details
                If (.settingListView = "Tile") Then style = View.Tile
                If (.settingListView = "SmallIcon") Then style = View.SmallIcon
                If (.settingListView = "LargeIcon") Then style = View.LargeIcon
                If (.settingListView = "List") Then style = View.List
            End With
            Return style
        End Get
        Set(value As View)
            With My.Settings
                .settingListView = value.ToString
                .Save()
            End With
        End Set
    End Property

    Public Property toolbarStyle As ToolStripItemDisplayStyle
        Get
            Dim style As ToolStripItemDisplayStyle
            With My.Settings
                If (.settingToolbarView = "Image") Then style = ToolStripItemDisplayStyle.Image
                If (.settingToolbarView = "ImageAndText") Then style = ToolStripItemDisplayStyle.ImageAndText
                If (.settingToolbarView = "Text") Then style = ToolStripItemDisplayStyle.Text
            End With

            Return style
        End Get
        Set(value As ToolStripItemDisplayStyle)
            With My.Settings
                .settingToolbarView = value.ToString
                .Save()
            End With
        End Set
    End Property

    Public Property startup As Boolean
        Get
            Return My.Settings.settingStartup
        End Get
        Set(value As Boolean)
            With My.Settings
                .settingStartup = value
                .Save()
            End With
        End Set
    End Property

    Public Property taskbar As Boolean
        Get
            Return My.Settings.settingShowInTaskBar
        End Get
        Set(value As Boolean)
            With My.Settings
                .settingToolbarView = value
                .Save()
            End With
        End Set
    End Property

    Public Property sortOrder As Boolean
        Get
            Return My.Settings.settingSortOrder
        End Get
        Set(value As Boolean)
            With My.Settings
                .settingSortOrder = value
                .Save()
            End With
        End Set
    End Property

    Public Property show As Boolean
        Get
            Return My.Settings.settingShowWindow
        End Get
        Set(value As Boolean)
            With My.Settings
                .settingshowWindow = value
                .Save()
            End With
        End Set
    End Property

    Public Sub boot()
        'load services from file
        IO.read(dataPath)
        'run startup services
        For Each service As Service In ServiceController.Index
            If service.startup = True Then service.Start()
        Next
    End Sub

    Public Sub save()
        IO.write(dataPath)
    End Sub

    Public Function dataPath() As String
        Return Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\services.bin"
    End Function

End Module
